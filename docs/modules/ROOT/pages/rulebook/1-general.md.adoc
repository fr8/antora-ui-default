:doctype: book

....

:CC-BY-NC-ND-2.5:
:section: 100
:revision: 19.20
....

= 1. RULES OF FREIGHT TRUST & CLEARING

== Definitions.

Unless the context clearly requires otherwise, the following terms shall have the following meanings when used in these Rules:

*Account* means a Customer Account or a Member Property Account, as context requires, used in conjunction with the trading or delivery of commodity products or smart contracts.
*Appeal Panel* means a panel comprised of individuals appointed by the Board or the Chief Compliance Officer to consider appeals under Rule 602.

*Applicable Law* means with respect to any Person, any statute, law, regulation, rule or ordinance of any governmental authority applicable to such Person, including the CEA, CFTC Regulations, and the rules or regulations of any relevant Self-Regulatory Organization.

*Approved Depository Institution* means a bank, trust company or other depository that has been approved by FreightTrust Clearing as an acceptable location for depositing Clearing Participant funds or Collateral, as applicable.

*Authorized Representative* means an individual designated by a Clearing Participant and registered with FreightTrust Clearing with authority to act on behalf of the Clearing Participant.

*Board* means the board of directors of FreightTrust Clearing, as set forth in the LLC Agreement.

*Business Day* means any day on which the Clearinghouse is open for clearing.
References in these Rules to a _day_ or  *Business Day* shall, unless the context otherwise requires, mean the "`Business Day** corresponding to the trading day of the Platform/Network.

*CEA* means the Commodity Exchange Act, as it may be amended from time to time.
_CFTC_ means the U.S.
Commodity Futures Trading Commission.

*CFTC Regulations* means the rules, regulations and interpretations promulgated by the CFTC pursuant to the CEA, as in effect from time to time.

*Chain* means the Freight Trust Network, _see Network_

*Clearinghouse* means FreightTrust Clearing LLC.

*Clearing Participant* means an FCM Participant or Direct Clearing Participant who has executed a Clearing Participant Agreement, and to whom the Clearinghouse has granted the right to clear contracts on the Network/Platform.

*Clearing Participant Agreement* means the agreement for clearing privileges between FreightTrust Clearing and a Clearing Participant.

*Collateral* means digital assets or other assets deposited by a Clearing Participant as from time to time determined by the Clearinghouse as acceptable in accordance with Rule 403(b).

*Committee* means a committee established by the Board or otherwise, pursuant to the Rules.

*Contract* means, as context requires, any forward contracts, futures contract, options contract or spot contract, agreement, or transaction on a commodity (as such term is defined in the CEA or CFTC Regulations), which has been approved for clearing by Freight Trust & Clearing pursuant to these Rules.

*Cryptocurrency* means any _Digital Asset_

*Customer* shall have the meaning set forth in CFTC Regulation 1.3(k).

*Customer Account* means an account established by an FCM Clearing Participant with FreightTrust Clearing in which the FCM Clearing Participant maintains trades, positions and Margin solely for Customers.

*Default* means, with respect to a Clearing Participant, if such Clearing Participant  	(a) fails to satisfy any of its Obligations,  	(b) fails to deliver funds within the time established therefor by Freight Trust & Clearing,  	(c) is expelled or suspended from the Exchange, FreightTrust Clearing or any Self-Regulatory Organization,  	(d) fails to meet the minimum capital and other financial requirements of FreightTrust Clearing, or  	(e) is Insolvent.

*Direct Clearing Participant* means a Person that submits trades for clearing at FreightTrust Clearing on behalf of its own account(s), has completed a Clearing Participant Agreement and has been granted clearing privileges by the Clearinghouse Proper.

*Directors* means members of the Board.

*Digital Asset* refers to but is not limited to Bitcoin ($XBT) Ethereum ($ETH), $BOL, or any Smart-Contract dervied Asset or UTXO asset.

*Disciplinary Panel* means a panel comprised of individuals appointed by the Board at the recommendation of the Chief Compliance Officer to act in an adjudicative role and fulfill various adjudicative responsibilities and duties described in Chapter 6.
_Emergency_ shall have the meaning set forth in Rule 207.

*Freight Trust* means FreightTrust & Clearing Corporation a Delaware C-Corporation.

*Exchange* means  (TBD).
and its respective successors.

*Exchange Committee* includes the Regulatory Oversight, Exchange Participant Committee, Nomination, or Exchange Practices Committees of the Exchange, and any other future or successor committee of the Exchange.

*EVM* refers to the _Ethereum Virtual Machine_

*FCM Clearing Participant* means a Person that is registered with the CFTC as a Futures Commission Merchant, has completed a Clearing Participant Agreement and has been granted clearing privileges by the Network/Clearinghouse.

*Fungible Tokens*

*Futures Commission Merchant* or _FCM_ shall have the meaning set forth in CFTC Regulation 1.3(p).

*Genesis File* refers to the https://github.com/freight-trust/spec/genesis.json[genesis.json] file.
This file determines the _Network Protocol_

*Government Agency* means the CFTC and/or any other governmental agency or department regulating the activities of a Clearing Participant.

*Insolvent* and *Insolvency* means the Clearing Participant has become the subject of a bankruptcy petition, receivership proceeding, or an equivalent proceeding.

*Margin* means funds or the applicable amount of the deliverable asset required to fully collateralize Contracts as set forth in Rule 403.

*Member Property Account* means an account established by a Clearing Participant with FreightTrust Clearing in which the Clearing Participant maintains trades, positions and Margin solely on its own behalf.

*Network* refers to the Blockchain Network generated through the _genisis file_

*Network Participant* means any user and/or service provider that transacts, provides services including but not limited to connectivity, block generation, concensus, API access, RPC access, or otherwises participates on-chain.

*Network Protocol* means the protocol initalized and specificed in the _genesis file_

*Node* means a service provider that provides, signs, validates or otherwise propergates blocks generated on the network.

*Non-Fungible Tokens*

*Obligations* means all financial obligations of a Clearing Participant, however arising, whether absolute or contingent, direct or indirect, due or to become to be due, arising under these Rules or such Clearing Participant's agreements with FreightTrust Clearing.

*Officer* has the meaning set forth in Rule 204.

*Person* means an individual, sole proprietorship, partnership, limited  liability company, association, firm, trust, corporation or other entity, as the context may require.

*Protocol* can refer to the _Network Protocol_ or a _Regime_

*Regulations* means rules, regulations, guidance, or advisories promulgated by the CFTC.

*Removal Event* means (a) the termination of the Clearing Participant Agreement;
(b) a materially false or misleading representation or warranty made by the Clearing Participant to FreightTrust Clearing under or in connection with any agreement between FreightTrust Clearing and the Clearing Participant;
(c) the breach by the Clearing Participant of the Rules or any of the terms or provisions of any agreement between FreightTrust Clearing and the Clearing Participant which is not remedied promptly after notice from FreightTrust Clearing;
(d) a material violation of the rules of the Exchange, or (e) a Default by the Clearing Participant.

*Rule* means a Rule of FreightTrust Clearing either contained in this Rulebook or in guidance or notices from FreightTrust Clearing.

*Self-Regulatory Organization* shall mean any futures or securities exchange, derivatives clearing organization, securities clearing agency, or National Futures Association.

*Settlement Price* has the meaning set forth in Rule 410.

*Smart Contract* means an executable program that runs on the network through the EVM.

*Transfer Trade* has the meaning set forth in Rule 408.

*UCC* means the Uniform Commercial Code as in effect in the State of Illinois, California, New York, and Delaware.
Interpretation deteremined by parties.

=== In these Rules, unless the context clearly requires otherwise,

....
(a) words in the singular include the plural and words in the plural include the singular,
(b) any gender includes each other gender,
(c) references to statutory provisions include those provisions, and any rules or regulations promulgated thereunder, as amended, and
(d) all uses of the word “including” should be construed to mean “including, but not limited to.

Headings included herein are for convenience purposes only and do not form a part of these Rules.
....

*Date and Time References* Unless otherwise specified, all references to dates, times or time periods shall refer to, or be measured in accordance with the time in New York City, New York.
